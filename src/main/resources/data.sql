DROP TABLE IF EXISTS ramp;

CREATE TABLE ramp (
  id VARCHAR(250) PRIMARY KEY,
  name VARCHAR(250) NOT NULL
);

INSERT INTO ramp (id, name) VALUES
  ('id1', 'Aliko'),
  ('id2', 'Bill'),
  ('id3', 'Folrunsho');
