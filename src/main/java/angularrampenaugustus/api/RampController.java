package angularrampenaugustus.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/rampen")
public class RampController {

        @Autowired
        private RampService rampService;

        @GetMapping()
        public ResponseEntity<List<Ramp>> getAllRampen() {
            List<Ramp> RampList = rampService.getAllRamps();
            return new ResponseEntity<>(RampList, HttpStatus.OK);
        }

        @GetMapping("/{id}")
        public ResponseEntity<Ramp> getRampById(
                @PathVariable("id") final String id) {
            Ramp Ramp = rampService.getRampById(id);
            return new ResponseEntity<>(Ramp, HttpStatus.OK);
        }

        @PostMapping()
        public ResponseEntity<Ramp> saveRamp(
                @RequestBody final UiRamp ramp) {
            String id = UUID.randomUUID().toString();
            Ramp savedRamp = rampService.saveRamp(new Ramp(id, ramp.getName()));
            return new ResponseEntity<>(savedRamp, HttpStatus.CREATED);
        }

        @PutMapping("/{id}")
        public ResponseEntity<Ramp> updateRampById(
                @PathVariable("id") final String id,
                @RequestBody final UiRamp ramp) {
            Ramp updatedRamp
                    = rampService.updateRampById(id, new Ramp(id, ramp.getName()));
            return new ResponseEntity<>(updatedRamp, HttpStatus.OK);
        }

        @DeleteMapping("/{id}")
        public ResponseEntity<String> deleteRampById(
                @PathVariable("id") final String id) {
            rampService.deleteRampById(id);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }

        @GetMapping("/search1/{searchString}")
        public ResponseEntity<List<Ramp>> getRampByNameContaining(
                @PathVariable("searchString") final String searchString) {
            List<Ramp> RampList
                    = rampService.getRampByNameContaining(searchString);
            return new ResponseEntity<>(RampList, HttpStatus.OK);
        }

        @GetMapping("/search2/{searchString}")
        public ResponseEntity<List<Ramp>> getRampByNameLike(
                @PathVariable("searchString") final String searchString) {
            List<Ramp> RampList
                    = rampService.getRampByNameLike(searchString);
            return new ResponseEntity<>(RampList, HttpStatus.OK);
        }
}
