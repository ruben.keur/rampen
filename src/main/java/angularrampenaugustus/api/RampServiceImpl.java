package angularrampenaugustus.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RampServiceImpl implements RampService {

    @Autowired
    private RampRepository RampRepository;

    @Override
    public List<Ramp> getAllRamps() {
        return (List<Ramp>) RampRepository.findAll();
    }

    @Override
    public Ramp getRampById(final String id) {
        return RampRepository.findById(id).get(0);
    }

    @Override
    public Ramp saveRamp(final Ramp Ramp) {
        return RampRepository.save(Ramp);
    }

    @Override
    public Ramp updateRampById(
            final String id, final Ramp RampToUpdate) {
        // Fetch the Ramp from db
        Ramp RampFromDb = RampRepository.findById(id).get(0);
        RampFromDb.setName(RampToUpdate.getName());
        return RampRepository.save(RampFromDb);
    }

    @Override
    public void deleteRampById(final String id) {
        RampRepository.deleteById(id);
    }

    @Override
    public List<Ramp> getRampByNameContaining(final String searchString) {
        return RampRepository.findByNameContaining(searchString);
    }

    @Override
    public List<Ramp> getRampByNameLike(final String searchString) {
        return RampRepository.findByNameLike(searchString);
    }
}
