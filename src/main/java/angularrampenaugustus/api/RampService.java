package angularrampenaugustus.api;

import java.util.List;

public interface RampService {
    List<Ramp> getAllRamps();

    Ramp getRampById(String id);

    Ramp saveRamp(Ramp Ramp);

    Ramp updateRampById(String id, Ramp RampToUpdate);

    void deleteRampById(String id);

    List<Ramp> getRampByNameContaining(String searchString);

    List<Ramp> getRampByNameLike(String searchString);
    
    
}
