package angularrampenaugustus.api;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface RampRepository extends CrudRepository<Ramp, Integer> {

    List<Ramp> findByNameContaining(String value);
    /**
     * Method to return List of Rampen with same substring in name.
     * Does the same job as above method, but different way of doing in Spring
     * @param value Value to check
     * @return List of Planel
     */
    @Query("SELECT p FROM Ramp p WHERE p.name LIKE %:value%")
    List<Ramp> findByNameLike(@Param("value") String value);

    @Query("SELECT p FROM Ramp p WHERE p.id LIKE %:value%")
    List<Ramp> findById(@Param("value") String id);

    @Transactional
    @Modifying
    @Query("DELETE FROM Ramp p WHERE p.id = :value")
    void deleteById(@Param("value") String id);
}
